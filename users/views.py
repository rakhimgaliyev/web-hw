from django.urls import reverse_lazy
from django.views import generic
from django.shortcuts import render, redirect, get_object_or_404

from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser

class SignUp(generic.CreateView):
	form_class = CustomUserCreationForm
	success_url = reverse_lazy('login')
	template_name = 'signup.html'

class EditProfile(generic.UpdateView):
	user = CustomUser
	fields = ('email', 'username', 'nickname',)
	template_name = 'settings.html'

	def get_object(self):
		return self.request.user

	def form_valid(self, form):
		user = form.save(commit=False)
		user.save()
		return redirect('home')
