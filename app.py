from pprint import pformat

def hello(env, start_response):
	start_response('200 OK', [('Content-Type', 'text/plain')])
	yield b'Hello world'

def application(env, start_response):
	status = '200 OK'
	body = pformat(env).encode('utf-8')

	headers = [
		('Content-Type', 'text/plain'),
		('Content-Type', str(len(body))),
	]

	start_response(status, headers)
	return [body]



# gunicorn --bind 127.0.0.1:8000 --workers 1 app:hello
