from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect

from . import models
from . import manager

class TagIndexView(ListView):
	model = models.Question
	paginate_by = 5
	template_name = 'home.html'

	def get_queryset(self):
		return manager.get_question_set_on_tag(self.kwargs.get('pk'))

	def get_context_data(self, **kwargs):
		context = super(TagIndexView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context
