from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect

from . import models
from . import manager

def set_right_answer_flag(request):
	user = request.user
	answer = manager.get_object(obj_list=models.Answer, pk=request.POST.get('answer_id'))
	if user.is_authenticated:
		if user == answer.question.author:
			if answer.is_right:
				answer.is_right = False
			else:
				answer.is_right = True
			answer.save()
	return(HttpResponseRedirect(answer.get_absolute_url()))

def like_question(request):
	user = request.user
	if user.is_authenticated:
		question = manager.get_object(obj_list=models.Question, pk=request.POST.get('question_id'))
		return manager.like_obj(obj=question, user=user)
	else:
		return HttpResponseRedirect('/users/login/')

def dislike_question(request):
	user = request.user
	if user.is_authenticated:
		question = manager.get_object(obj_list=models.Question, pk=request.POST.get('question_id'))
		return manager.dislike_obj(obj=question, user=user)
	else:
		return HttpResponseRedirect('/users/login/')

def like_answer(request):
	user = request.user
	if user.is_authenticated:
		answer = manager.get_object(obj_list=models.Answer, pk=request.POST.get('answer_id'))
		return manager.like_obj(obj=answer, user=user)
	else:
		return HttpResponseRedirect('/users/login/')

def dislike_answer(request):
	user = request.user
	if user.is_authenticated:
		answer = manager.get_object(obj_list=models.Answer, pk=request.POST.get('answer_id'))
		return manager.dislike_obj(obj=answer, user=user)
	else:
		return HttpResponseRedirect('/users/login/')
