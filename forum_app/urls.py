from django.urls import path
from . import views, questions_view, tags_view, answers_view

urlpatterns = [
	path('', questions_view.HomePageView.as_view(), name='home'),
	path('hot/', questions_view.HotPageView.as_view(), name='hot'),
	path('ask/', questions_view.QuestionCreateView.as_view(), name='ask'),

	path('<int:pk>/', questions_view.QuestionDetailView.as_view(), name='question_detail'),
	path('<int:pk>/edit/', questions_view.QuestionUpdateView.as_view(), name='question_edit'),
	path('<int:pk>/delete/', questions_view.QuestionDeleteView.as_view(), name='question_delete'),

	path('<int:pk>/answer/', answers_view.AnswerCreateView.as_view(), name='answer'),
	path('<int:pk>/answer/edit/', answers_view.AnswerUpdateView.as_view(), name='answer_edit'),
	path('<int:pk>/answer/delete/', answers_view.AnswerDeleteView.as_view(), name='answer_delete'),

	path('like', views.like_question, name='like_question'),
	path('dislike', views.dislike_question, name='dislike_question'),

	path('answer/like/', views.like_answer, name='like_answer'),
	path('answer/dislike', views.dislike_answer, name='dislike_answer'),
	path('answer/set_right_answer_flag/', views.set_right_answer_flag, name='set_right_answer_flag'),

	path('tag/<int:pk>/', tags_view.TagIndexView.as_view(), name='tagview'),
]
