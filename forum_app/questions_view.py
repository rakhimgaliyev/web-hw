from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.utils import timezone

from . import models
from . import manager


class HomePageView(ListView):
	model = models.Question
	paginate_by = 20
	template_name = 'home.html'
	ordering = ['-date']

	def get_context_data(self, **kwargs):
		context = super(HomePageView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context


class HotPageView(ListView):
	model = models.Question
	paginate_by = 20
	template_name = 'home.html'
	ordering = ['-raiting', '-date']

	def get_context_data(self, **kwargs):
		context = super(HotPageView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context

class QuestionDetailView(DetailView):
	model = models.Question
	template_name = 'question_detail.html'
	login_url = 'login'

	def get_context_data(self, **kwargs):
		context = super(QuestionDetailView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context

class QuestionCreateView(LoginRequiredMixin, CreateView):
	model = models.Question
	template_name = 'ask.html'
	fields = ['title', 'body', 'tags_string',]
	login_url = 'login'
	
	def form_valid(self, form):
		form.instance.author = self.request.user
		form.instance.date = timezone.now()
		kwargs = super(QuestionCreateView, self).get_form_kwargs()
		tags_string = kwargs.__getitem__('data').__getitem__('tags_string')
		form.instance.tags_string = ""
		tags_lst = tags_string.replace(',', ' ').split()
		tags_lst = list(set(tags_lst))
		all_tags = manager.get_tags_set()
		form.save()
		tags = []
		tags_string = ''
		for tag in tags_lst:
			if not all_tags.filter(name=tag).exists():
				tag_obj = models.Tag(name=tag)
				tag_obj.save()
			tags_string = tags_string + ' ' +  tag + ','
			tags.append(all_tags.filter(name=tag).first())
		if len(tags) > 0:
			form.instance.tags.add(*tags)
		if tags_string != '':
			form.instance.tags_string = tags_string
		form.save()
		return HttpResponseRedirect(form.instance.get_absolute_url())

	def get_context_data(self, **kwargs):
		context = super(QuestionCreateView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context


class QuestionUpdateView(LoginRequiredMixin, UpdateView):
	model = models.Question
	fields = ['title', 'body', 'tags_string',]
	template_name = 'question_edit.html'
	login_url = 'login'

	def form_valid(self, form):
		form.instance.tags.all().delete()
		form.instance.date = timezone.now()
		kwargs = super(QuestionUpdateView, self).get_form_kwargs()
		tags_string = kwargs.__getitem__('data').__getitem__('tags_string')
		form.instance.tags_string = ""
		tags_lst = tags_string.replace(',', ' ').split()
		tags_lst = list(set(tags_lst))
		all_tags = manager.get_tags_set()
		form.save()
		tags = []
		tags_string = ''
		for tag in tags_lst:
			if not all_tags.filter(name=tag).exists():
				tag_obj = models.Tag(name=tag)
				tag_obj.save()
			tags_string = tags_string + ' ' +  tag + ','
			tags.append(all_tags.filter(name=tag).first())
		if len(tags) > 0:
			form.instance.tags.add(*tags)
		if tags_string != '':
			form.instance.tags_string = tags_string
		form.save()
		return HttpResponseRedirect(form.instance.get_absolute_url())

	def get_context_data(self, **kwargs):
		context = super(QuestionUpdateView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context

class QuestionDeleteView(LoginRequiredMixin, DeleteView):
	model = models.Question
	template_name = 'question_delete.html'
	success_url = reverse_lazy('home')
	login_url = 'login'

	def get_context_data(self, **kwargs):
		context = super(QuestionDeleteView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context
