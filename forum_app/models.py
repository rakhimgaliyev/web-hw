from django.db import models
from django.urls import reverse
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation

from . import manager

class Tag(models.Model):
	name = models.CharField(max_length=200, unique=True)

	objects = models.Manager()


class Question(models.Model):
	title = models.CharField(max_length=200, blank=True)
	body = models.TextField(max_length=1000)
	date = models.DateTimeField()
	raiting = models.IntegerField(default=0)

	tags = models.ManyToManyField(
		Tag,
		related_name='questions',
		blank=True
	)
	tags_string = models.CharField(max_length=200, blank=True)

	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	likes = models.ManyToManyField(
		settings.AUTH_USER_MODEL,
		related_name='question_likes',
		blank=True
	)
	dislikes = models.ManyToManyField(
		settings.AUTH_USER_MODEL,
		related_name='question_dislikes',
		blank=True
	)

	objects = models.Manager()

	def __str__(self):
		return self.title

	def get_absolute_url(self):
		return reverse('question_detail', args=[str(self.id)])

	def get_answers(self):
		return self.answers.all()


class Answer(models.Model):
	body = models.TextField(max_length=1000)
	date = models.DateTimeField()
	raiting = models.IntegerField(default=0)
	is_right = models.BooleanField(default=False)

	author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
	likes = models.ManyToManyField(
		settings.AUTH_USER_MODEL,
		related_name='answer_likes',
		blank=True
	)
	dislikes = models.ManyToManyField(
		settings.AUTH_USER_MODEL,
		related_name='answer_dislikes',
		blank=True
	)
	question = models.ForeignKey(
		Question,
		on_delete=models.CASCADE,
		related_name='answers'
	)

	objects = models.Manager()

	def __str__(self):
		return self.text

	def get_absolute_url(self):
		return reverse('question_detail', args=[str(self.question.pk)])

