from django.db import models

from django.shortcuts import render, get_object_or_404
from django.http.response import HttpResponseRedirect
from django.utils import timezone
from django.db.models import Count

from . import models 
def get_question_tag(pk):
	return Question.objects.get(pk=pk).tags.all()

def get_popular_tags():
	return
	return models.Tag.objects.annotate(q_count=Count('questions')).order_by('-q_count')[:10]

def update_popular_tags():
	try:
		if (timezone.now() - popularTags.lastUpdate).seconds > popularTags.updatePeriod:
			tags = models.Tag.objects.annotate(q_count=Count('questions')).order_by('-q_count')[:10]
			print(tags)
			popularTags.lastUpdate = timezone.now()
			popularTags.save()
	except popularTags.DoesNotExist:
		return

def get_object(obj_list, pk):
	return get_object_or_404(obj_list, pk=pk)

def get_question_set_on_tag(pk):
	return models.Question.objects.filter(tags=pk).order_by('-raiting', '-date')

def is_liked(obj, user):
	return obj.likes.filter(pk=user.id).exists()

def is_disliked(obj, user):
	return obj.dislikes.filter(pk=user.id).exists()

def like_obj(obj, user):
	if obj.likes.filter(pk=user.id).exists():
		obj.raiting -= 1
		obj.likes.remove(user)
	else:
		if obj.dislikes.filter(pk=user.id).exists():
			obj.dislikes.remove(user)
			obj.raiting += 1
		obj.raiting += 1
		obj.likes.add(user)
	obj.save()
	return HttpResponseRedirect(obj.get_absolute_url())

def dislike_obj(obj, user):
	if obj.dislikes.filter(pk=user.id).exists():
		obj.raiting += 1
		obj.dislikes.remove(user)
	else:
		if obj.likes.filter(pk=user.id).exists():
			obj.raiting -= 1
			obj.likes.remove(user)
		obj.raiting -= 1
		obj.dislikes.add(user)
	obj.save()
	return HttpResponseRedirect(obj.get_absolute_url())
