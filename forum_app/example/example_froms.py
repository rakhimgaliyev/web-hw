class LoginForm(forms.Form):
	username = forms.CharField()
	password = forms.CharField(widget=forms.PasswordInput)

	def clean_timezone(self):
		timezone = self.cleaned_data.get("timezone")
		if "?" in timezone:
			raise forms.ValidationError("Без ?")
		return timezone

	def clean_username(self):
		username = self.cleaned_data.get("username")
		if "!" in username:
			raise forms.ValidationError("Без !")
		return username
