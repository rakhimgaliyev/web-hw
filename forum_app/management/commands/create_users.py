from django.core.management.base import BaseCommand

from users.models import CustomUser

from mixer.backend.django import mixer

class Command(BaseCommand):
	def handle(self, *args, **options):
		if options['count']:
			count = options['count']
			if count < 1:
				print("Can't create negative count of users")
				return
			if count > 10000:
				print("Can't create more than 10000 users")
				return
			users = []
			for i in range (count):
				fullName = mixer.faker.name()
				username = fullName.replace(' ', '') + str(mixer.faker.small_positive_integer())
				nickname = fullName.split(' ')[0] + str(mixer.faker.small_positive_integer())
				password = mixer.faker.password()
				email = mixer.faker.email()
				users.append(CustomUser(email=email, username=username, nickname=nickname, password=password))
				print(str(i + 1) + " user created")
			print("Beginning to save users to database...")
			CustomUser.objects.bulk_create(users, len(users))
			print("Success")
		else:
			import this

	def add_arguments(self, parser):
		parser.add_argument('count', nargs='?', type=int)
