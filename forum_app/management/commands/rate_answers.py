from django.core.management.base import BaseCommand

from users.models import CustomUser
from forum_app.models import Answer

from random import randint

class Command(BaseCommand):
	def handle(self, *args, **options):
		if options['count']:
			count = options['count']
			if count < 1:
				print("can't create negative count of likes and dislikes")
				return
			if count > 1000000:
				print("Can't create more than 1000000 answer per answer")
				return
			users_count = CustomUser.objects.count()
			if (users_count <= 0):
				return
			answers_count = Answer.objects.count()
			if (answers_count <= 0):
				return
			a_first = Answer.objects.first().pk
			a_last = Answer.objects.last().pk
			likes_and_dislikes_created = 0
			for i in range(count):
				try:
					user_id = randint(0, users_count-1)
					if not CustomUser.objects.filter(pk=user_id).exists():
						continue
					user = CustomUser.objects.get(pk=user_id)
					answers_like_lst = []
					answers_dislike_lst = []
					num_of_likes_and_dislikes = randint(1, 1000)
					if (num_of_likes_and_dislikes <= 200):
						continue
					if (num_of_likes_and_dislikes <= 400):
						num_of_likes_and_dislikes = randint(1, 10)
					elif (num_of_likes_and_dislikes <= 850):
						num_of_likes_and_dislikes = num_of_likes_and_dislikes % 3
					for j in range (num_of_likes_and_dislikes):
						answer_id = randint(a_first, a_last)
						if (Answer.objects.filter(pk=answer_id).exists()):
							answer = Answer.objects.get(pk=answer_id)
							x = randint(1, 10)  # 1: dislike    3-4: like
							if x <= 4:
								if not user.answer_dislikes.filter(pk=answer.id).exists():
									answers_dislike_lst.append(answer)
									likes_and_dislikes_created = likes_and_dislikes_created + 1
							if x >=5:
								if not user.answer_likes.filter(pk=answer.id).exists():
									answers_like_lst.append(answer)
									likes_and_dislikes_created = likes_and_dislikes_created + 1
					answers_dislike_lst = list(set(answers_dislike_lst))
					user.answer_dislikes.add(*answers_dislike_lst)

					answers_like_lst = list(set(answers_like_lst))
					user.answer_likes.add(*answers_like_lst)

					print("Created m2m for " + str(i) + " user")
				except CustomUser.DoesNotExist:
		    			answer = None
				if (count <= likes_and_dislikes_created):
					break

	def add_arguments(self, parser):
		parser.add_argument('count', nargs='?', type=int)
