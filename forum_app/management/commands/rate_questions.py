from django.core.management.base import BaseCommand

from users.models import CustomUser
from forum_app.models import Question

from random import randint

class Command(BaseCommand):
	def handle(self, *args, **options):
		if options['count']:
			count = options['count']
			if count < 1:
				print("can't create negative count of likes and dislikes")
				return
			if count > 1000000:
				print("Can't create more than 1000000 answer per question")
				return
			users_count = CustomUser.objects.count()
			if (users_count <= 0):
				return
			questions_count = Question.objects.count()
			if (questions_count <= 0):
				return
			q_first = Question.objects.first().pk
			q_last = Question.objects.last().pk
			likes_and_dislikes_created = 0
			for i in range(count):
				try:
					question = Question.objects.get(pk=randint(0, questions_count-1))
					users_like_lst = []
					users_dislike_lst = []
					num_of_likes_and_dislikes = randint(1, 1000)
					if (num_of_likes_and_dislikes <= 200):
						continue
					if (num_of_likes_and_dislikes <= 400):
						num_of_likes_and_dislikes = randint(1, 10)
					elif (num_of_likes_and_dislikes <= 850):
						num_of_likes_and_dislikes = num_of_likes_and_dislikes % 3
					for j in range (num_of_likes_and_dislikes):
						u_first = CustomUser.objects.first().pk
						u_last = CustomUser.objects.last().pk
						author_id = randint(u_first, u_last)
						if (CustomUser.objects.filter(pk=author_id).exists()):
							user = CustomUser.objects.get(pk=author_id)
							x = randint(1, 10)  # 1: dislike    3-4: like
							if x <= 4:
								if not question.dislikes.filter(pk=user.id).exists():
									users_dislike_lst.append(user)
									likes_and_dislikes_created = likes_and_dislikes_created + 1
							if x >=5:
								if not question.likes.filter(pk=user.id).exists():
									users_like_lst.append(user)
									likes_and_dislikes_created = likes_and_dislikes_created + 1
					users_dislike_lst = list(set(users_dislike_lst))
					question.dislikes.add(*users_dislike_lst)

					users_like_lst = list(set(users_like_lst))
					question.likes.add(*users_like_lst)

					print("Created m2m for " + str(i) + " question")
				except Question.DoesNotExist:
		    			question = None
				if (count <= likes_and_dislikes_created):
					break

	def add_arguments(self, parser):
		parser.add_argument('count', nargs='?', type=int)
