from django.core.management.base import BaseCommand

from users.models import CustomUser
from forum_app.models import Answer, Question

from random import randint
from django.utils import timezone
from mixer.backend.django import mixer

class Command(BaseCommand):
	def handle(self, *args, **options):
		if options['count']:
			count = options['count']
			if count < 1:
				print("can't create negative count of answers")
				return
			if count > 1000000:
				print("Can't create more than 1000000 answers")
				return
			users_count = CustomUser.objects.count()
			users = CustomUser.objects.all()
			if users_count < 1:
				print("Can't create questions because users do not exist")
				return
			questions = Question.objects.all()
			questions_count = Question.objects.count()
			if questions_count < 1:
				print("Can't create questions because questions do not exist")
				return

			answers = []
			for i in range (count):
				question_num = randint(0, questions_count - 1)
				dt = timezone.timedelta(randint(0, 1000))
				date = timezone.now() - dt	
				body = mixer.faker.text()
				author_id = randint(0, users_count - 1)
				author = users[author_id]
				answers.append(Answer(date=date, body=body, author=author, question=questions[question_num]))
				print(str(i + 1) + " answer created")
				if (i % 1000 == 0) and (len(answers) > 0):
					print("saving " + str(len(answers)) + " answers to database")
					Answer.objects.bulk_create(answers, len(answers))
					answers = []
			if (i % 1000 == 0) and (len(answers) > 0):
				print("saving " + str(len(answers)) + " answers to database")
				Answer.objects.bulk_create(answers, len(answers))
			print("Success")
		else:
			import this

	def add_arguments(self, parser):
		parser.add_argument('count', nargs='?', type=int)
