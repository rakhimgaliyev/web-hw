from django.core.management.base import BaseCommand

from forum_app.models import Tag

from mixer.backend.django import mixer

class Command(BaseCommand):
	def handle(self, *args, **options):
		if options['count']:
			count = options['count']
			if count < 1:
				print("can't create negative count of tags")
				return
			if count > 10000:
				print("can't create more than 10000 tags")
				return
			tags = []
			for i in range (count):
				name = mixer.faker.genre() + mixer.faker.slug()
				if not Tag.objects.filter(name=name).exists() and not (name in tags):
					tags.append(Tag(name=name))
					print(str(i + 1) + " tag created")
				if (i % 1000 == 0) and (len(tags) > 1):
					print("saving " + str(len(tags)) + " tags to database")
					Tag.objects.bulk_create(tags, len(tags))
					tags = []
			if (len(tags) != 0):
				print("saving " + str(len(tags)) + " tags to database")
				Tag.objects.bulk_create(tags, len(tags))
			print("Success")
		else:
			import this

	def add_arguments(self, parser):
		parser.add_argument('count', nargs='?', type=int)
