from django.core.management.base import BaseCommand

from users.models import CustomUser
from forum_app.models import Question

from random import randint
from django.utils import timezone
from mixer.backend.django import mixer

class Command(BaseCommand):
	def handle(self, *args, **options):
		if options['count']:
			count = options['count']
			if count < 1:
				print("can't create negative count of questions")
				return
			if count > 100000:
				print("Can't create more than 100000 questions")
				return
			users_count = CustomUser.objects.count()
			if users_count < 1:
				print("Can't create questions because users do not exist")
				return
			questions = []
			users = CustomUser.objects.all()
			for i in range (count):
				dt = timezone.timedelta(randint(0, 1000))
				date = timezone.now() - dt
				title = mixer.faker.slug()
				body = mixer.faker.text()
				author_id = randint(0, users_count - 1)
				author = users[author_id]
				questions.append(Question(date=date, title=title, body=body, author=author))
				print(str(i + 1) + " question created")
				if (i % 1000 == 0) and (len(questions) > 0):
					print("saving " + str(len(questions)) + " questions to database")
					Question.objects.bulk_create(questions, len(questions))
					questions = []

			if (len(questions) > 0):
				print("saving " + str(len(questions)) + " questions to database")
				Question.objects.bulk_create(questions, len(questions))
			print("Success")
		else:
			import this

	def add_arguments(self, parser):
		parser.add_argument('count', nargs='?', type=int)
