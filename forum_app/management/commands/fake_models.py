from django.conf import settings
from forum_app.models import Question, Answer, Tag
from users.models import CustomUser
from random import randint
from mixer.backend.django import mixer
from django.utils import timezone

def make_users(count=10):
	if count <= 1:
		print("Can't create negative count of users or 0, 1 user")
		return
	if count > 10000:
		print("Can't create more than 10000 users")
		return
	users = []
	for i in range (count):
		fullName = mixer.faker.name()
		username = fullName.replace(' ', '') + str(mixer.faker.small_positive_integer())
		nickname = fullName.split(' ')[0] + str(mixer.faker.small_positive_integer())
		password = mixer.faker.password()
		email = mixer.faker.email()
		users.append(CustomUser(email=email, username=username, nickname=nickname, password=password))
		print(str(i) + " user created")
	print("Beginning to save users to database...")
	CustomUser.objects.bulk_create(users, len(users))
	print("Success")
	return

def make_tags(count=10):
	if count <= 1:
		print("can't create negative count of tags or 0, 1 tag")
		return
	if count > 10000:
		print("can't create more than 1000 tags")
		return
	tags = []
	for i in range (count + 2):
		name = mixer.faker.genre() + mixer.faker.slug()
		if not Tag.objects.filter(name=name).exists() and not (name in tags):
			tags.append(Tag(name=name))
			print(str(i) + " tag created")
		if (i % 1000 == 0) and (len(tags) > 1):
			print("saving 1000 tags to database")
			Tag.objects.bulk_create(tags, len(tags))
			tags = []
	if (tags != []):
		print("saving " + str(len(tags)) + " tags to database")
		Tag.objects.bulk_create(tags, len(tags))
		print("Success")
	return

def make_questions(count=10):
	if count <= 1:
		print("can't create negative count of questions")
		return
	if count > 100000:
		print("Can't create more than 100000 questions")
		return
	users_count = CustomUser.objects.count()
	if users_count <= 1:
		print("Can't create questions because users do not exist")
		return
	questions = []
	users = CustomUser.objects.all()
	for i in range (count):
		dt = timezone.timedelta(randint(0, 1000))
		date = timezone.now() - dt
		title = mixer.faker.slug()
		body = mixer.faker.text()
		author_id = randint(0, users_count - 1)
		author = users[author_id]
		questions.append(Question(date=date, title=title, body=body, author=author))
		print(str(i + 1) + " question created")
		if (i % 1000 == 0):
			print("saving 1000 questions to database")
			Question.objects.bulk_create(questions, len(questions))
			questions = []

	print("saving 1000 questions to database")
	Question.objects.bulk_create(questions, len(questions))
	print("Success")
	return

def make_tag_question_relationship():
	print("Start to add m2m relationships with tags")
	questions = list(Question.objects.all())
	tags = Tag.objects.all()
	tags_count = Tag.objects.count()
	questions_count = Question.objects.count()
	for i in range (2243, tags_count):
		tags_questions = []
		for j in range(randint(2, 10)):
			tag_id = randint(0, tags_count - 1)
			tags_questions.append(questions[tag_id])
		tags[i].questions.add(*tags_questions)
		print("Created m2m for " + str(i) + " tag")
	print("Success")
	return

def make_answers(count=10):
	if count < 1:
		print("can't create negative count of answers")
		return
	if count > 1000000:
		print("Can't create more than 1000000 answers")
		return
	users_count = CustomUser.objects.count()
	users = CustomUser.objects.all()
	if users_count < 1:
		print("Can't create questions because users do not exist")
		return
	questions = Question.objects.all()
	questions_count = Question.objects.count()
	if questions_count < 1:
		print("Can't create questions because questions do not exist")
		return

	answers = []
	for i in range (count):
		question_num = randint(0, questions_count - 1)
		dt = timezone.timedelta(randint(0, 1000))
		date = timezone.now() - dt	
		body = mixer.faker.text()
		author_id = randint(0, users_count - 1)
		author = users[author_id]
		answers.append(Answer(date=date, body=body, author=author, question=questions[question_num]))
		print(str(i) + " answer created")
		if (i % 1000 == 0):
			print("saving 1000 answers to database")
			Answer.objects.bulk_create(answers, len(answers))
			answers = []
	print("saving 1000 answers to database")
	Answer.objects.bulk_create(answers, len(answers))
	print("Success")
	return


def make_questions_raiting(count = 1000):
	if count < 1:
		print("can't create negative count of likes and dislikes")
		return
	if count > 1000000:
		print("Can't create more than 1000000 answer per question")
		return
	users_count = CustomUser.objects.count()
	if (users_count <= 0):
		return
	questions_count = Question.objects.count()
	if (questions_count <= 0):
		return
	q_first = Question.objects.first().pk
	q_last = Question.objects.last().pk
	likes_and_dislikes_created = 0
	for i in range(count):
		try:
			question = Question.objects.get(pk=randint(0, questions_count-1))
			users_like_lst = []
			users_dislike_lst = []
			num_of_likes_and_dislikes = randint(1, 1000)
			if (num_of_likes_and_dislikes <= 200):
				continue
			if (num_of_likes_and_dislikes <= 400):
				num_of_likes_and_dislikes = randint(1, 10)
			elif (num_of_likes_and_dislikes <= 850):
				num_of_likes_and_dislikes = num_of_likes_and_dislikes % 3
			for j in range (num_of_likes_and_dislikes):
				u_first = CustomUser.objects.first().pk
				u_last = CustomUser.objects.last().pk
				author_id = randint(u_first, u_last)
				if (CustomUser.objects.filter(pk=author_id).exists()):
					user = CustomUser.objects.get(pk=author_id)
					x = randint(1, 10)  # 1: dislike    3-4: like
					if x <= 4:
						if not question.dislikes.filter(pk=user.id).exists():
							users_dislike_lst.append(user)
							likes_and_dislikes_created = likes_and_dislikes_created + 1
					if x >=5:
						if not question.likes.filter(pk=user.id).exists():
							users_like_lst.append(user)
							likes_and_dislikes_created = likes_and_dislikes_created + 1
			users_dislike_lst = list(set(users_dislike_lst))
			question.dislikes.add(*users_dislike_lst)

			users_like_lst = list(set(users_like_lst))
			question.likes.add(*users_like_lst)

			print("Created m2m for " + str(i) + " question")
		except Question.DoesNotExist:
    			question = None
		if (count <= likes_and_dislikes_created):
			break

def make_answers_raiting(count = 1000):
	if count < 1:
		print("can't create negative count of likes and dislikes")
		return
	if count > 1000000:
		print("Can't create more than 1000000 answer per answer")
		return
	users_count = CustomUser.objects.count()
	if (users_count <= 0):
		return
	answers_count = Answer.objects.count()
	if (answers_count <= 0):
		return
	a_first = Answer.objects.first().pk
	a_last = Answer.objects.last().pk
	likes_and_dislikes_created = 0
	for i in range(count):
		try:
			user_id = randint(0, users_count-1)
			if not CustomUser.objects.filter(pk=user_id).exists():
				continue
			user = CustomUser.objects.get(pk=user_id)
			answers_like_lst = []
			answers_dislike_lst = []
			num_of_likes_and_dislikes = randint(1, 1000)
			if (num_of_likes_and_dislikes <= 200):
				continue
			if (num_of_likes_and_dislikes <= 400):
				num_of_likes_and_dislikes = randint(1, 10)
			elif (num_of_likes_and_dislikes <= 850):
				num_of_likes_and_dislikes = num_of_likes_and_dislikes % 3
			for j in range (num_of_likes_and_dislikes):
				answer_id = randint(a_first, a_last)
				if (Answer.objects.filter(pk=answer_id).exists()):
					answer = Answer.objects.get(pk=answer_id)
					x = randint(1, 10)  # 1: dislike    3-4: like
					if x <= 4:
						if not user.answer_dislikes.filter(pk=answer.id).exists():
							answers_dislike_lst.append(answer)
							likes_and_dislikes_created = likes_and_dislikes_created + 1
					if x >=5:
						if not user.answer_likes.filter(pk=answer.id).exists():
							answers_like_lst.append(answer)
							likes_and_dislikes_created = likes_and_dislikes_created + 1
			answers_dislike_lst = list(set(answers_dislike_lst))
			user.answer_dislikes.add(*answers_dislike_lst)

			answers_like_lst = list(set(answers_like_lst))
			user.answer_likes.add(*answers_like_lst)

			print("Created m2m for " + str(i) + " user")
		except CustomUser.DoesNotExist:
    			answer = None
		if (count <= likes_and_dislikes_created):
			break

def correct_questions_raiting():
	questions_count = Question.objects.count()
	if (questions_count <= 0):
		return
	a_first = Question.objects.first().pk
	a_last = Question.objects.last().pk
	questions = list(Question.objects.all())
	for i in range(len(questions)):
		if (questions[i].raiting == questions[i].likes.count() - questions[i].dislikes.count()):
			continue
		questions[i].raiting = questions[i].likes.count() - questions[i].dislikes.count()
		questions[i].save()
		print("corrected raiting for " + str(i) + " question")


def correct_answers_raiting():
	answers_count = Answer.objects.count()
	if (answers_count <= 0):
		return
	a_first = Answer.objects.first().pk
	a_last = Answer.objects.last().pk
	answers = list(Answer.objects.all())
	for i in range(len(answers)):
		if (answers[i].raiting == answers[i].likes.count() - answers[i].dislikes.count()):
			continue
		answers[i].raiting = answers[i].likes.count() - answers[i].dislikes.count()
		answers[i].save()
		print("corrected raiting for " + str(i) + " answer")









