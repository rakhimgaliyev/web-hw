from django.core.management.base import BaseCommand

from forum_app.models import Answer

from random import randint

class Command(BaseCommand):
	def handle(self, *args, **options):
		answers_count = Answer.objects.count()
		if (answers_count <= 0):
			return
		a_first = Answer.objects.first().pk
		a_last = Answer.objects.last().pk
		answers = list(Answer.objects.all())
		for i in range(len(answers)):
			if (answers[i].raiting == answers[i].likes.count() - answers[i].dislikes.count()):
				continue
			answers[i].raiting = answers[i].likes.count() - answers[i].dislikes.count()
			answers[i].save()
			print("corrected raiting for " + str(i) + " answer")


