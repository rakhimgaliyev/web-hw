from django.core.management.base import BaseCommand

from forum_app.models import Question, Tag

from random import randint

class Command(BaseCommand):
	def handle(self, *args, **options):
		print("Start to add m2m relationships with tags")
		questions = list(Question.objects.all())
		tags = Tag.objects.all()
		tags_count = Tag.objects.count()
		questions_count = Question.objects.count()
		for i in range (2243, tags_count):
			tags_questions = []
			for j in range(randint(2, 10)):
				tag_id = randint(0, tags_count - 1)
				tags_questions.append(questions[tag_id])
			tags[i].questions.add(*tags_questions)
			print("Created m2m for " + str(i + 1) + " tag")
		print("Success")

