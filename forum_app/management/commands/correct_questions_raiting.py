from django.core.management.base import BaseCommand

from forum_app.models import Question

from random import randint

class Command(BaseCommand):
	def handle(self, *args, **options):
		questions_count = Question.objects.count()
		if (questions_count <= 0):
			return
		a_first = Question.objects.first().pk
		a_last = Question.objects.last().pk
		questions = list(Question.objects.all())
		for i in range(len(questions)):
			if (questions[i].raiting == questions[i].likes.count() - questions[i].dislikes.count()):
				continue
			questions[i].raiting = questions[i].likes.count() - questions[i].dislikes.count()
			questions[i].save()
			print("corrected raiting for " + str(i) + " question")
