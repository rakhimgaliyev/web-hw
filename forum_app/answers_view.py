from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from django.shortcuts import render, get_object_or_404
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponseRedirect
from django.utils import timezone

from . import models
from . import manager


class AnswerCreateView(LoginRequiredMixin, CreateView):
	model = models.Answer
	template_name = 'answer.html'
	fields = ['body',]
	login_url = 'login'

	def form_valid(self, form):
		form.instance.date = timezone.now()
		form.instance.author = self.request.user
		form.instance.question = manager.get_object(obj_query=models.Question, pk=self.kwargs['pk'])
		return super().form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(AnswerCreateView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context

class AnswerUpdateView(LoginRequiredMixin, UpdateView):
	model = models.Answer
	fields = ['body', ]
	template_name = 'question_edit.html'
	login_url = 'login'

	def form_valid(self, form):
		form.instance.date = timezone.now()
		return super().form_valid(form)

	def get_context_data(self, **kwargs):
		context = super(AnswerUpdateView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context

class AnswerDeleteView(LoginRequiredMixin, DeleteView):
	model = models.Answer
	template_name = 'question_delete.html'
	success_url = reverse_lazy('home')
	login_url = 'login'

	def get_context_data(self, **kwargs):
		context = super(AnswerDeleteView, self).get_context_data(**kwargs)
		context['popularTags'] = manager.get_popular_tags()
		return context
